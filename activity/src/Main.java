import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try{
            System.out.println("Input an Integer whose factorial will be computed:");
            int num = in.nextInt();
            int answer = 1;
            int counter = 1;

            if (num < 0) {
                System.out.println("Factorial function can't be used for negative integer.");
            } else if (num == 0) {
                System.out.println("Factorial of 0 is 1.");
            } else {
              while (counter <= num) {
                  answer *= counter;
                  counter++;
              }
              System.out.println("Factorial of " + num + " using While loop is: " + answer);

              answer = 1;
              for (int i = 1; i <= num; i++){
                  answer*=i;
              }
              System.out.println("Factorial of " + num + " using for loop is: " + answer);
            }
        } catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }

    }
}